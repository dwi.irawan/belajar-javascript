var Excel = require('exceljs');
var wb = new Excel.Workbook();

wb.xlsx.readFile("./excel.xlsx").then(function(){

    var sh = wb.getWorksheet("Sheet1");
  
    sh.getRow(3).getCell(2).value = 32;

    // Save
    wb.xlsx.writeFile("./excel.xlsx");
    console.log("Row-3 | Cell-2 - "+sh.getRow(3).getCell(2).value);

    // Get last row
    console.log(sh.rowCount);
    //Get all the rows data [1st and 2nd column]
    for (i = 1; i <= sh.rowCount; i++) { 
        console.log(sh.getRow(i).getCell(1).value);
        console.log(sh.getRow(i).getCell(2).value);
    }

    // Function
    var nilai = sh.getRow(1).getCell(3).value
    console.log('Hasil rumus: '+nilai.result)
});